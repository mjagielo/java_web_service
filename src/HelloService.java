
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Created by mike on 11/29/15.
 */
@WebService(targetNamespace = "http://test")
public class HelloService {
  @WebMethod
  public String sayHelloWorldFrom(String from) {
    String result = "Hello, world, from " + from;
    System.out.println(result);
    return result;
  }
  public static void main(String[] argv) {
    Object implementor = new HelloService ();
    String address = "http://localhost:9000/HelloService";
    Endpoint.publish(address, implementor);
  }
}
